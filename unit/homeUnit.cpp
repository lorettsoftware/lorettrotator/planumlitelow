// Maybe it doesn't work =D
// Need check
#include <Arduino.h>
#include "Station.h"

#define AZ_END_PIN 2
#define AZ_DIR_PIN 3
#define AZ_STEP_PIN 4
#define AZ_EN_PIN 5

#define EL_END_PIN 10
#define EL_DIR_PIN 11
#define EL_STEP_PIN 12
#define EL_EN_PIN 13

// Инициализация моторов
Station station(AZ_STEP_PIN, AZ_DIR_PIN, EL_STEP_PIN, EL_DIR_PIN, AZ_END_PIN, EL_END_PIN);

void setup() {
    Serial2.begin(115200);
  
    pinMode(AZ_END_PIN, INPUT);
    pinMode(EL_END_PIN, INPUT);
}

void loop() {
    //station.YouSpinMeRound();
    station.findHome();
    delay(1000);
    station.navigateRel(-90, 90);
    delay(100);
}
