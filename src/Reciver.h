#ifndef PLANUM_RECIVER_03112022
#define PLANUM_RECIVER_03112022

#include <Arduino.h>
#include <GParser.h>
#include "Station.h"
#include "Power.h"
//#include "IMU.h"

char divider = ' ';
char ending = ';';
char start = '$';

const int buffSize = 256;
char buff[buffSize];

float azimuth = 0;
float elevation = 0;

void readCommand(Station& station, PowerControl& power) {
    if (Serial.available() > 3) {
        // Perhaps it is enough to put only at the end, but I will overwrite everything
        for(int i=0; i < buffSize; ++i) {
            buff[i] = '\0';
        }

        int chars = 0;
        bool startDetect=false;
        while((Serial.available() > 0) && (chars < buffSize)) {
            char let = Serial.read();

            if (let == '\n')
                break;
            if (let == start)
                startDetect=true;

            if (startDetect) {
                buff[chars] = let;
                ++chars;
            }
            
            delay(10); // don't delete this. UART transfer maybe brocken
        }

        GParser data = GParser(buff, divider);
        
        int count = data.split();
        
        
        if (data.equals(0, "$res;")) {
            status = SLEEP;
        }
        
        else if (data.equals(0, "$pow;")) {
            float v = power.getVoltage();
            float c = power.getCurrent();
            float av = power.getAnalogV();
            float ac = power.getAnalogC();
            
            Serial.print(v);
            Serial.print(" ");
            Serial.print(c);
            Serial.print(" ");
            Serial.print(av);
            Serial.print(" ");
            Serial.println(ac);
        }

        else if (status != HOMEERROR) {
            if (data.equals(0, "$h;")){
                station.findHome();
            }
            else if (data.equals(0, "$stop;")) {
                status = HOLD;
                station.stop();
            }
            
            else if (data.equals(0, "$cb;")) {
                status = MOVE;
                station.comeback();
            }

            else if (data.equals(0, "$s;")) {
                station.setHome();
            }

            /*
            else if(data.equals(0, "$getOr;")) {
                float yaw = imu.getYawDeg();
                float pitch = imu.getPitchDeg();
                float roll = imu.getRollDeg();

                Serial.printf("%f|%f|%f\n", yaw, pitch, roll);
            }

            else if(data.equals(0, "$getTel;")) {
                float yaw = imu.getYawDeg();
                float pitch = imu.getPitchDeg();
                float roll = imu.getRollDeg();

                float altitude = imu.getAltitude();
                float temperature = imu.getTemperature();

                Serial.printf("%f|%f|%f|%f|%f\n", yaw, pitch, roll, altitude, temperature);
            }
            */

            else if (data.equals(0, "$c;")) {
                station.clearCorrections();
            }

            else if (data.equals(0, "$n")) {
                azimuth = data.getFloat(1);
                elevation = data.getFloat(2);

                station.navigate(azimuth, elevation);
                
            }

            else if (data.equals(0, "$sdp")) {
                int16_t azimuthDirection = data.getInt(1);
                int16_t elevationDirection = data.getInt(2);
                bool status = station.setDirectionPriority(azimuthDirection, elevationDirection);
                if (status) 
                    Serial.println("DIR_OK");
                else
                    Serial.println("DIR_IGNORE");
                //station.navigate(azimuth, elevation, true);
                
            }
            
            else if (data.equals(0, "$nf")) {
                azimuth = data.getFloat(1);
                elevation = data.getFloat(2);

                
                station.navigate(azimuth, elevation, true);
                
            }

            else if (data.equals(0, "$nr")) {
                azimuth = data.getFloat(1);
                elevation = data.getFloat(2);

                if (azimuth != 0 || elevation != 0) {
                    status = MOVEREL;
                    station.navigateRel(azimuth, elevation);
                }
            }

            else if (data.equals(0, "$nrm")) {
                azimuth = data.getFloat(1);
                elevation = data.getFloat(2);

                if (azimuth != 0 || elevation != 0) {
                    status = MOVE;
                    station.navigateRelMath(azimuth, elevation);
                }
            }

            else if (data.equals(0, "$nrc")) {
                azimuth = data.getFloat(1);
                elevation = data.getFloat(2);

                if (azimuth != 0 || elevation != 0) {
                    status = MOVE;
                    station.navigateRel(azimuth, elevation);
                }
            }
        }

    }
}

#endif