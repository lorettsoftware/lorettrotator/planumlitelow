#include <Arduino.h>
#include <Wire.h>
#include "Station.h"
#include "Reciver.h"
#include "Pinout.h"
#include "Power.h"


//#include "IMU.h"

#define COMMAND_WAIT_TIME 100 // in ms

STATUS status = SLEEP;

unsigned long commandReadTimer = millis();

Station station(AZ_STEP_PIN, AZ_DIR_PIN, EL_STEP_PIN, EL_DIR_PIN, AZ_END_PIN, EL_END_PIN);
PowerControl power(VOLTAGE_PIN, AMPERE_PIN);
//IMU imu(WIRE_SDA, WIRE_SCL);
//HardwareSerial* connection = &Serial1;

// Timeout that prevents the program from hanging completely if the end sensor malfunctions
int timeout = 360 / station.getSpeed() * 2 * 1000;
bool blink = true;
unsigned long blinkTimer = millis();
unsigned long powerTimer = millis();

void setup() {   
    Wire1.setSCL(15);
    Wire1.setSDA(14);
    Wire1.begin();
    
    Serial.begin(115200);

    analogReadResolution(12);

    pinMode(EL_END_PIN, INPUT_PULLUP);
    pinMode(AZ_END_PIN, INPUT);

    pinMode(AMPLIFEIR_SWITCH, OUTPUT);
    pinMode(LED_BUILTIN, OUTPUT);

    //pinMode(intr, INPUT_PULLUP);
    //attachInterrupt(digitalPinToInterrupt(intr), setup, CHANGE);
    
    digitalWrite(AMPLIFEIR_SWITCH, 1);

    
}

void loop() {
    
    if(Serial.available() > 3 && millis() - commandReadTimer > COMMAND_WAIT_TIME) {
        readCommand(station, power);
        commandReadTimer = millis();
    }

    
    if ((millis() - blinkTimer) > 500) {
        digitalWrite(LED_BUILTIN, blink);
        blink=!blink;
        blinkTimer = millis();

        //Serial.println(digitalRead(AZ_END_PIN));
    }

    if ((millis() - powerTimer) > 200) {
        power.tick();
        powerTimer = millis();
    }
    
}

void loop1() {
    station.tick();
}