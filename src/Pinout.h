#pragma once

const uint16_t WIRE_SDA  = 14;
const uint16_t WIRE_SCL = 15;

const uint16_t UART_RX = 13;
const uint16_t UART_TX = 12;

const uint16_t SERIAL_RX = 9;
const uint16_t SERIAL_TX = 8;

const uint16_t AMPLIFEIR_SWITCH = 11;

const uint16_t AMPERE_PIN = 26;
const uint16_t VOLTAGE_PIN = 27;

const uint16_t AZ_DIR_PIN = 4;
const uint16_t AZ_STEP_PIN = 5;
const uint16_t AZ_EN_PIN = 6;
const uint16_t AZ_END_PIN = 7;

const uint16_t EL_DIR_PIN = 0;
const uint16_t EL_STEP_PIN = 1;
const uint16_t EL_EN_PIN = 2;
const uint16_t EL_END_PIN = 3;

const uint16_t LED_PIN = 10;